# Basic styling
Inspired from here: <https://github.com/drewtempelmeyer/palenight.vim>

## Colors

|                   |           |
|-------------------|-----------|
| Background        | 0x292d3e  |
| Background alt    | 0x3e4452  |
| Text              | 0xbfc7d5  |
| Text alt          | 0x697098  |
| Blue              | 0x82b1ff  |
| Light blue        | 0x89ddff  |
| Purple            | 0xc792ea  |
| Blue purple       | 0x939ede  |


## Spaces
General margin: 16px or 8px
Tabs: 32px / 16px

