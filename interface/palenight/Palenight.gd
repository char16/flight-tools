extends Node

const background: Color = Color("#292d3e")
const backgroundAlt: Color = Color("#3e4452")
const text: Color = Color("bfc7d5")
const textAlt: Color = Color("697098")

const blue: Color = Color("#82b1ff")
const lightBlue: Color = Color("#89ddff")
const bluePurple: Color = Color("#939ede")
const purple: Color = Color("#c792ea")

const font: DynamicFont = preload("res://assets/fonts/FontRegular.tres")
