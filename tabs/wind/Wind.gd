extends Control

var heading: int
var direction: int
var speed: int

var alpha: int
var crosswind: float
var headTailwind: float

## Maths
func _crosswind() -> void:
    crosswind =  speed * -sin(deg2rad(alpha))

func _headTailwind() -> void:
    headTailwind = speed * cos(deg2rad(alpha + 180))

func _alpha() -> void:
    var x = abs(heading - direction)
    var y = (360 - heading + direction)
    alpha = min(x, y) as int

## Programm
func calculate() -> void:
    var hd = $Input/Heading.text
    if not hd.is_valid_integer(): return
    elif hd as int < 0 or hd as int > 360: return
    else: heading = hd as int

    var dr = $Input/Direction.text
    if not dr.is_valid_integer(): return
    elif dr as int < 0 or hd as int > 360: return
    else: direction = dr as int

    var sd = $Input/Speed.text
    if not sd.is_valid_integer(): return
    elif sd as int < 0: return
    else: speed = sd as int

    _alpha()
    _crosswind()
    _headTailwind()

    render()

func render() -> void:
    $Output/Key/Crosswind.text = "Crosswind: %s knots" % stepify(crosswind, 0.01)
    if headTailwind <= 0:
        $Output/Key/headTailwind.text = "Headwing: %s knots" % stepify(headTailwind, 0.01)
    else: $Output/Key/headTailwind.text = "Tailwind: %s knots" % stepify(headTailwind, 0.01)

    ##TODO draw on plot

func _on_Submit() -> void:
    calculate()

func _input(event: InputEvent) -> void:
    if event.is_action_pressed("ui_accept"):
        calculate()
